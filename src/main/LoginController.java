package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginController implements Initializable{
	@FXML private TextField username;
	@FXML private PasswordField password;
	@FXML private Button login;
	@FXML private Button exit;
	@FXML private Text redMessage;
	
	private Socket socket;
    private BufferedReader serverResponse;
    private PrintWriter loginDataProvider;
    
    private static final String loginStatus = "Succes!";
    
    /* (non-Javadoc)
     * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
     * Text appear when you failed connecting
     */
    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	
	try {
		socket = new Socket("localhost", 5000);
		serverResponse = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		loginDataProvider =  new PrintWriter(socket.getOutputStream(), true);
	} catch (IOException e) {
        System.out.println("Can't connect to server.");
    }
	login.setOnMouseClicked(event ->{
		if (username.getText() != null && password.getText() != null) {
			String username2 = username.getText();
			String password2 = password.getText();
			
			loginDataProvider.println(username2);
			loginDataProvider.println(password2);
			
			try {
				if(serverResponse.readLine().equals(loginStatus)) {
					Scene currentScene=login.getScene();
					currentScene.getWindow().hide();
					
					FXMLLoader fxmlLoader = new FXMLLoader();
					fxmlLoader.setLocation(getClass().getResource("/main/Controller.fxml"));
					Scene scene = new Scene(fxmlLoader.load());
					Stage stage = new Stage();
					stage.setTitle("PetShop");
					stage.setScene(scene);
					stage.show();
				}
				else {
					redMessage.setOpacity(1);
				}
			}catch (IOException e) {
		    	System.out.println("Failed to login!");
		    }
		}
	});
	exit.setOnAction(event -> Platform.exit());
	}
}
