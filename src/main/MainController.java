package main;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Base64;
import java.util.List;
import java.util.ResourceBundle;

import dao.ProgramariDao;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML
	private ListView<String> listView;
	@FXML
	private ListView<String> istoricListView;
	@FXML
	private BorderPane borderPane;
	@FXML
	private TextField nameId;
	@FXML
	private TextField breedId;
	@FXML
	private TextField ownerId;
	@FXML
	private TextField ageId;
	@FXML
	private TextField weightId;
	@FXML
	private ImageView imageId;

	@FXML
	private void Second(MouseEvent event) throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUP();
		db.startTransaction();
		List<Programare> programari = (List<Programare>) db.programareList();
		int load = listView.getSelectionModel().getSelectedIndex();
		nameId.setText(programari.get(load).getAnimal().getName());
		breedId.setText(programari.get(load).getAnimal().getBreed());
		ageId.setText("" + programari.get(load).getAnimal().getAge());
		ownerId.setText(programari.get(load).getAnimal().getOwner());
		weightId.setText("" + programari.get(load).getAnimal().getWeight());
		
		if (programari.get(load).getAnimal().getWeight() > 99) {
			weightId.setText("" + programari.get(load).getAnimal().getWeight() + "(Bun de taiat)");
		}
		
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
				Base64.getDecoder().decode(programari.get(load).getAnimal().getAnimalImage()));
		Image image = new Image(byteArrayInputStream);
		imageId.setImage(image);
		getIstoric(programari.get(load).getAnimal().getIdAnimal());

		// Add image in database
		// AnimalDao animalDao = new AnimalDao(db);
		// animalDao.setImageToAnimal(programari.get(load).getAnimal().getIdAnimal(),
		// "C:\\Users\\John Doe\\Eclipse\\Projects\\PetShop\\resource\\"+
		// programari.get(load).getAnimal().getName()+".jpg");

	}

	public void populateMainListView() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUP();
		db.startTransaction();
		List<Programare> programariDBList = (List<Programare>) db.programareList();
		// List<Animal> animalDBList = (List<Animal>) db.animalList();
		ObservableList<Programare> programariList = FXCollections.observableArrayList(programariDBList);
		listView.setItems(getProgramare(programariList));
		listView.refresh();
		db.closeEntityManager();
	}

	private void getIstoric(final int idAnimal) {
		DatabaseUtil db = new DatabaseUtil();
		ProgramariDao programariDao = new ProgramariDao(db);
		List<Programare> istoricProgramari = (List<Programare>) programariDao.istoricProgramareList(idAnimal);
		ObservableList<Programare> istoricProgramariList = FXCollections.observableArrayList(istoricProgramari);
		istoricListView.setItems(getProgramare(istoricProgramariList));
		istoricListView.refresh();
	}

	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		animals.stream().forEach(a -> names.add(a.getName()));
		return names;
	}

	public ObservableList<String> getProgramare(List<Programare> programari) {
		ObservableList<String> aux = FXCollections.observableArrayList();
		programari.stream().forEach(p -> aux.add(p.getDateToString() + "/" + p.getHourToString() + "/" + p.getType()));
		return aux;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					System.out.println("Programare: " + newValue);
				}
			}
        });
	}
}
