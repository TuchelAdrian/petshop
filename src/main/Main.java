package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import util.DatabaseGeneric;
import util.DatabaseUtil;

/**
 * Main class
 *
 */
public class Main extends Application {

//	/**
//	 * This method start the program
//	 * @param args
//	 * @throws Exception
//	 */
//	private static Stage primaryStage;
//	
//	public static void main(String[] args) throws Exception {
//		DatabaseUtil dbUtil = new DatabaseUtil();
//		AnimalDao animalDao=new AnimalDao(dbUtil);
//		PersonalMedicalDao personalMedicalDao=new PersonalMedicalDao(dbUtil);
//		ProgramariDao programariDao=new ProgramariDao(dbUtil);
//		/*bob.setIdAnimal(1);
//		bob.setName("Alma");
//		dbUtil.setUP();
//		dbUtil.startTransaction();
//		dbUtil.saveAnimal(bob);
//		//Aici se salveaza cu adevarat in baza de date
//		dbUtil.commitTransaction();
//		dbUtil.printAllAnimalsFromDB();
//		dbUtil.closeEntityManager();
//		Animal vali=new Animal();
//		vali.setIdAnimal(2);
//		vali.setName("Vali");
//		animalDao.create(vali);*/
//		List<Animal> animale=animalDao.readAll();
//		for (Animal animal : animale) {
//			System.out.println(animal);
//		}
//		List<Programare> programari = programariDao.readAll();
//		Collections.sort(programari);
//		
//		for (Programare programare : programari) {
//			System.out.println(programare);
//		}
//	}
//
//	@Override
//	public void start(Stage primaryStage) {
//		try {
//			this.setPrimaryStage(primaryStage);
//			setPrimaryStage(primaryStage);
//			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("Sample.fxml"));
//			Scene scene = new Scene(root,400,400);
//			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
//			primaryStage.setScene(scene);
//			primaryStage.show();
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	public static Stage getPrimaryStage() {
//		return primaryStage;
//	}
//
//	public void setPrimaryStage(Stage primaryStage) {
//		Main.primaryStage = primaryStage;
//	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			AnchorPane root=(AnchorPane) FXMLLoader.load(getClass().getResource("/main/Login.fxml"));
			//BorderPane root=(BorderPane) FXMLLoader.load(getClass().getResource("/main/Controller.fxml"));
			Scene scene = new Scene(root,300,150);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}catch(Exception e) {
		e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception {
		launch(args);	
	}
}
