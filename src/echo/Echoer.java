package echo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Echoer extends Thread{
	private Socket socket;
	
	private static String username = "root";
	private static String password = "toor";
	
	public Echoer(Socket socket) {
		this.socket=socket;
	}
	
	public void run() {
		try {
			BufferedReader input= new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output= new PrintWriter(socket.getOutputStream(), true);
				String user=input.readLine();
				String pass=input.readLine();
				
				while(true) {
				if(user.equals(Echoer.username) && pass.equals(Echoer.password))
						output.println("Succes!");
					else 
						output.println("Failed!");
			}
		}catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
