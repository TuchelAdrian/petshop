package util;

public class DatabaseGeneric<T extends DatabaseUtil> {
	
	private static DatabaseGeneric<DatabaseUtil> instance;
	
	static {
		try {
			instance=new DatabaseGeneric<>();
		}catch(Exception e) {
			System.out.println("Error");
		}
	}
	public static DatabaseGeneric<DatabaseUtil> getInstance()
	{
		return instance;
	}
	
	public DatabaseGeneric() throws Exception{
		DatabaseUtil.setUP();
		DatabaseUtil.startTransaction();
		DatabaseUtil.commitTransaction();
	}
	
	public void addToDatabase(DatabaseGeneric<T> db) {
		DatabaseUtil.entityManager.persist(db);
	}
	
	public void closeEntity() {
		DatabaseUtil.closeEntityManager();
	}
}
