package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dao.AnimalDao;
import model.Animal;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {
	// For connection
	public static EntityManagerFactory entityManagerFactory;
	// For transactions
	public static EntityManager entityManager;

	/**
	 * @throws Exception
	 */
	public static void setUP() throws Exception {
		new Persistence();
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShop");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}
	public void savePersonalmedical(Personalmedical personalmedical) {
		entityManager.persist(personalmedical);
	}
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	public static void startTransaction() {
		entityManager.getTransaction().begin();
	}
	public static void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	//Function that close the entity
	public static void closeEntityManager() {
		entityManager.close();
	}
	//Function for print results from database
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery
				("Select * from petshop.animal", Animal.class).getResultList();
		for(Animal animal : results) {
			System.out.println("Animal :" + animal.getName() + " has ID: " + animal.getIdAnimal());
		}
	}
	public List<Animal> animalList(){
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a", Animal.class).getResultList();
		return animalList;
	}
	
	public List<Programare> programareList(){
		List<Programare> programareList = (List<Programare>)entityManager.createQuery("SELECT a FROM Programare a WHERE a.date >= CURRENT_DATE order by a.date", Programare.class).getResultList();
		return programareList;
	}
}
