package dao;

import java.util.List;

import model.Personalmedical;
import util.DatabaseUtil;

public class PersonalMedicalDao extends DatabaseUtil{
	DatabaseUtil databaseUtil;

	public PersonalMedicalDao(DatabaseUtil databaseUtil) {
		this.databaseUtil = databaseUtil;
	}
	//Create
	public Personalmedical create(Personalmedical entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			databaseUtil.savePersonalmedical(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
		return entity;
	}
	//Update
	public Personalmedical update(Personalmedical entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			DatabaseUtil.entityManager.merge(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
		return entity;
	}
	//Delete
	public void delete(Personalmedical entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			entity = DatabaseUtil.entityManager.find(Personalmedical.class, entity.getIdPersonalMedical());
			DatabaseUtil.entityManager.remove(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
		} finally {
			databaseUtil.closeEntityManager();
		}
	}
	//Read
	public Personalmedical findById(int id) {
		try {
			databaseUtil.setUP();
			return DatabaseUtil.entityManager.find(Personalmedical.class, id);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
	}
	//ReadAll
	public List<Personalmedical> readAll() {
		try {
			databaseUtil.setUP();
			return DatabaseUtil.entityManager.createNativeQuery("Select * from Personalmedical", Personalmedical.class)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
	}
}
