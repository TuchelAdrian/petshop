package dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Animal;
import model.Personalmedical;

class DaoTest {

	@Test
	void test() {
		Animal animal = new Animal();
		animal.setName("Marcel");		
		assertEquals("Marcel", animal.getName());
	}
	@Test
	void test2() {
		Animal animal = new Animal();
		animal.setAge(2);
		assertEquals(2, animal.getAge());
	}
	@Test
	void test3() {
		Animal animal = new Animal();
		animal.setBreed("gaina de teste");
		assertEquals("gaina de teste", animal.getBreed());
	}
	@Test
	void test4() {
		Personalmedical medic = new Personalmedical();
		assertNotNull(medic);
	}
	@Test
	void test5() {
		Animal animal = new Animal();
		assertNotNull(animal);
	}
}
