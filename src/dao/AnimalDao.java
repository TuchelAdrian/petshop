package dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

import model.Animal;
import util.DatabaseUtil;

public class AnimalDao extends DatabaseUtil{
	DatabaseUtil databaseUtil;

	public AnimalDao(DatabaseUtil databaseUtil) {
		this.databaseUtil = databaseUtil;
	}
	/**
	 * Create
	 * @param entity
	 * @return
	 */
	public Animal create(Animal entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			databaseUtil.saveAnimal(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
		return entity;
	}
	/**
	 * Update
	 * @param entity
	 * @return
	 */
	public Animal update(Animal entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			DatabaseUtil.entityManager.merge(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
		return entity;
	}
	/**
	 * Delete
	 * @param entity
	 */
	public void delete(Animal entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			entity = DatabaseUtil.entityManager.find(Animal.class, entity.getIdAnimal());
			DatabaseUtil.entityManager.remove(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
		} finally {
			databaseUtil.closeEntityManager();
		}

	}
	/**
	 * Find animal in database
	 * @param id
	 * @return Animal if is in database
	 */
	public Animal findById(int id) {
		try {
			databaseUtil.setUP();
			return DatabaseUtil.entityManager.find(Animal.class, id);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
	}
	//ReadAll
	public List<Animal> readAll() {
		try {
			databaseUtil.setUP();
			return DatabaseUtil.entityManager.createNativeQuery("Select * from PetShop.animal", Animal.class).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
	}
	
	/**
	 * @param idAnimal
	 * @param pathImage
	 */
	public void setImageToAnimal(final int idAnimal, final String pathImage) {
		Path pathImg = Paths.get(pathImage);
		Animal animal = findById(idAnimal);
		byte[] content = null;
		if (Objects.nonNull(animal)) {
			try {
				content = Files.readAllBytes(pathImg);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(content)) {
				String encodedImg = Base64.getEncoder().encodeToString(content);
				animal.setAnimalImage(encodedImg);
				update(animal);
			}
		}
	}
}
