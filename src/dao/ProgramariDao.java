package dao;

import java.util.List;

import javax.persistence.Query;

import model.Animal;
import model.Programare;
import util.DatabaseUtil;

public class ProgramariDao extends DatabaseUtil{
	DatabaseUtil databaseUtil;

	public ProgramariDao(DatabaseUtil databaseUtil) {
		this.databaseUtil = databaseUtil;
	}
	//Create
	public Programare create(Programare entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			databaseUtil.saveProgramare(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
		return entity;
	}
	//Update
	public Programare update(Programare entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			DatabaseUtil.entityManager.merge(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
		return entity;
	}
	//Delete
	public void delete(Programare entity) {
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			entity = DatabaseUtil.entityManager.find(Programare.class, entity.getIdprogramare());
			DatabaseUtil.entityManager.remove(entity);
			databaseUtil.commitTransaction();
		} catch (Exception ex) {
			ex.printStackTrace();
			DatabaseUtil.entityManager.getTransaction().rollback();
		} finally {
			databaseUtil.closeEntityManager();
		}

	}
	//Read
	public Programare findById(int id) {
		try {
			databaseUtil.setUP();
			return DatabaseUtil.entityManager.find(Programare.class, id);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
	}
	//ReadAll
	public List<Programare> readAll() {
		try {
			databaseUtil.setUP();
			return DatabaseUtil.entityManager.createNativeQuery("Select * from Programare", Programare.class).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
	}

	public List<Programare> istoricProgramareList(int idAnimal) {
		AnimalDao animalDao = new AnimalDao(databaseUtil);
		Animal animal = animalDao.findById(idAnimal);
		try {
			databaseUtil.setUP();
			databaseUtil.startTransaction();
			Query query = DatabaseUtil.entityManager
					.createQuery("SELECT a FROM Programare a WHERE a.date < CURRENT_DATE AND a.animal=:selected");
			query.setParameter("selected", animal);
			List<Programare> programareList = (List<Programare>) query.getResultList();
			return programareList;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			databaseUtil.closeEntityManager();
		}
	}
}
