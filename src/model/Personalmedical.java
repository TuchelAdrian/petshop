package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idPersonalMedical;

	private String functie;

	private String name;

	public Personalmedical() {
	}

	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

	public String getFunctie() {
		return this.functie;
	}

	public void setFunctie(String functie) {
		this.functie = functie;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}