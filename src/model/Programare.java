package model;

import java.io.Serializable;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable, Comparable<Programare> {
	private static final long serialVersionUID = 1L;

	@Id
	private int idprogramare;

	@Temporal(TemporalType.DATE)
	private Date date;

	private Time hour;

	private String type;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="idPersonal")
	private Personalmedical personalmedical;

	public Programare() {
	}

	public int getIdprogramare() {
		return this.idprogramare;
	}

	public void setIdprogramare(int idprogramare) {
		this.idprogramare = idprogramare;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getHour() {
		return this.hour;
	}

	public void setHour(Time hour) {
		this.hour = hour;
	}

	public String getType() {
		return this.type;
	}

	public void setType(Date type) {
		this.type = setDaySpecificator(type);
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}
	
	public String getDateToString() {
		return setDaySpecificator(this.date);
	}

	public String getHourToString() {
		String aux;
		aux=this.hour.toString();
		return aux;
	}

	/**Datetime format
	 * @param aDay
	 * @return
	 */
	private String setDaySpecificator(Date aDay) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = format.format(aDay.getTime());
		return date;
	}
	
	@Override
	public String toString() {
		return "Programare [idprogramare=" + idprogramare + ", date=" + date + ", hour=" + hour + ", type=" + type
				+ ", animal=" + animal + ", personalmedical=" + personalmedical + "]";
	}

	@Override
	public int compareTo(Programare o) {
		return this.date.compareTo(o.date);
	}

}